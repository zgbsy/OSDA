## 打包

软件打包使用 `Inno` 工具，点击 [此处](http://www.jrsoftware.org/isinfo.php) 下载安装。

### 支持语言

* 英文
* 中文

    中文语言包点击 [此处](http://www.jrsoftware.org/files/istrans/) （`Inno` 官方网站），选择 `Chinese (Simplified)` 对应文件下载到 `PATH/Languages/` 目录下即可。

## 图标作者
BlackVariant (Patrick)

## 图标作者网站
http://blackvariant.deviantart.com/art/Button-UI-Requests-12-520378397

## 图标使用说明
Free for non-commercial use