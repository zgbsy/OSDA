OSDA Project Documentation
##################################

OSDA 是以 `MIT`_ 协议授权的串行端口调试助手（可以在 `Gitee`_ 或者 `Github`_ 仓库中找到。）。

.. _MIT :
   https://gitee.com/leven9/OSDA/blob/master/LICENSE

.. _Gitee :
   https://gitee.com/leven9/OSDA

.. _Github :
   https://github.com/leven99/OSDA

.. figure:: _images/osda.png
   :width: 750px
   :align: center

.. toctree::
   :maxdepth: 1

   getting_started.rst
   contribute.rst
   guides.rst