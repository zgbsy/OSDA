# 概述

文档采用 `Sphinx` 生成，`reStructuredText` 书写。

# 主题安装

如果计算机没有安装 `sphinx_ustack_theme`，请执行以下命令安装主题。

```
pip install sphinx_ustack_theme
```

# 文档生成

* For Windows. PowerShell

```
PS> cd OSDA/Docs
PS> .\make.bat html
```